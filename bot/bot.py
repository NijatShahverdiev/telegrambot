import requests
import json
from io import BytesIO
import base64


def sendPhoto(token, chat_id, img_url, info):
    print("Detected...")
    bot_url = f'https://api.telegram.org/bot{token}/sendPhoto'
    remote_image = requests.get(img_url)
    photo = BytesIO(remote_image.content)
    files = {'photo': photo}
    data = {'chat_id': chat_id, 'caption': info}
    r = requests.post(bot_url, files=files, data=data)
    print(r.status_code, r.ok)


def sendMediaGroup(token, chat_id, urls, info):
    params = {'chat_id': chat_id, 'media': []}
    for url in urls:
        params['media'].append({'type': 'photo', 'media': url})
    caption = {'caption': info}
    params['media'][0].update(caption)
    params['media'] = json.dumps(params['media'])
    bot_url = f'https://api.telegram.org/bot{token}/sendMediaGroup'
    r = requests.get(bot_url, data=params)
    print(params)
    print(r.content)

token = '1568624773:AAHvNDKWaDn3FLFYuzuByJPA4bJufQdhbT4'
chat_id ="-535872565"
urls = ['http://ipAddress/api/lp5/6/samples/faces/d29f65db-abf5-4326-a47d-b332d2bfe343',
            'http://ipAddress/api/lp5/6/samples/faces/d29f65db-abf5-4326-a47d-b332d2bfe34']

if __name__=="__main__":
    sendMediaGroup(token, chat_id, urls,'Test Data')
    
